class ChangesRestaurantOpeningHoursToStartAndEnd < ActiveRecord::Migration
  def change
  	add_column :restaurants, :opening, :time
  	add_column :restaurants, :closing, :time
  	remove_column :restaurants, :opening_hours
  end
end
