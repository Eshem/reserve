class ChangeMinAndMaxToPersons < ActiveRecord::Migration
  def change
    rename_column :tables, :max_occupants, :persons
    remove_column :tables, :min_occupants
  end
end
