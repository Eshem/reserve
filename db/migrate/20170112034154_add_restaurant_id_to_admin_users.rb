class AddRestaurantIdToAdminUsers < ActiveRecord::Migration
  def change
  	add_column :admin_users, :restaurant_id, :integer
  end
end
