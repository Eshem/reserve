class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :table_id
      t.datetime :time
      t.integer :group_size

      t.timestamps null: false
    end
  end
end
