# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# AdminUser.create!(email: 'admin@ex.com', password: 'password', password_confirmation: 'password')

Restaurant.create(name: 'Olivo', opening: Time.parse('12pm'), closing: Time.parse('10pm'))
Restaurant.create(name: 'Oliveto', opening: Time.parse('12pm'), closing: Time.parse('10pm'))
Restaurant.create(name: 'Olivomare', opening: Time.parse('12pm'), closing: Time.parse('10pm'))
Restaurant.create(name: 'Olivocarne', opening: Time.parse('12pm'), closing: Time.parse('10pm'))

Table.create(name: nil, persons: 4, restaurant_id: 1)
# Reservation.create(table_id: 1, time: Time.parse('6pm'), group_size: 2)