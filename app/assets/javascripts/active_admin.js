#= require active_admin/base

$(document).ready(function(){
  $('#reservation_user_id').change(function(){
    if($(this).val() == 'New'){
      $('.user-fields').show();
      $('#reservation_user_attributes_name').prop('disabled','')
      $('#reservation_user_attributes_email').prop('disabled','')
    } else {
      $('.user-fields').hide();
      $('#reservation_user_attributes_name').prop('disabled','disabled')
      $('#reservation_user_attributes_email').prop('disabled','disabled')
    }
  })
})