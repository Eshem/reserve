ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation, :restaurant_id

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :email
      row :current_sign_in_at
      row :sign_in_count
      row ('Restaurant') { |o| link_to Restaurant.find(o.restaurant_id).try(:name), admin_restaurant_path(o.restaurant_id) }
      row :created_at
    end
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :restaurant_id, as: :select, collection: Restaurant.all.map { |u| [u.name, u.id] }
    end
    f.actions
  end

end
