ActiveAdmin.register Reservation do
  scope_to :scoped_collection
  permit_params :group_size, :table_id, :time, :restaurant_id, :user_id, user_attributes: [:name, :email]

  form do |f|
    inputs 'Details' do
      input :group_size, as: :number, as: :select, collection: (1..12)
      input :time, as: :datetime_picker
      input :restaurant_id, input_html: { value: current_admin_user.restaurant_id }, as: :hidden
      input :table_id, label: 'Table', as: :select, collection: Table.all.map { |u| ["Table: #{u.name}, #{u.persons} people", u.id] }
      input :user_id, label: 'User', as: :select, collection: User.all.map { |u| [u.name, u.id] }.unshift(['New User', 'New'])
    end
    f.inputs "User", for: :user_attributes, class: 'user-fields' do |user|
      user.input :name
      user.input :email
    end
    actions
  end

  controller do
    def scoped_collection
      current_admin_user.restaurant_id ? Reservation.where(restaurant_id: current_admin_user.restaurant_id) : Reservation.all
    end
  end

end
