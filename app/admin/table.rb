ActiveAdmin.register Table do
  scope_to :scoped_collection

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :persons, :restaurant_id

  form do |f|
    inputs 'Details' do
      input :name
      input :persons, as: :number, as: :select, collection: (1..12)
      input :restaurant_id, input_html: { value: current_admin_user.restaurant_id }, as: :hidden
    end
    actions
  end

  controller do
    def scoped_collection
      current_admin_user.restaurant_id ? Table.where(restaurant_id: current_admin_user.restaurant_id) : Table.all
    end
  end

end
