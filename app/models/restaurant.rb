class Restaurant < ActiveRecord::Base
	has_many :tables
	has_many :reservations
	belongs_to :user

	validates_uniqueness_of :name
end
