class User < ActiveRecord::Base
  has_many :reservations

  validates_presence_of :name, :email
  validates_uniqueness_of :email
end
