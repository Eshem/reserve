class Reservation < ActiveRecord::Base
	belongs_to :restaurant
	belongs_to :user
	belongs_to :table
  accepts_nested_attributes_for :user
  validates_presence_of :table_id, :time, :group_size, :restaurant_id

  scope :today, -> { where("time >= ?", Time.zone.now.beginning_of_day) }
end
