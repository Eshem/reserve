class Table < ActiveRecord::Base
	belongs_to :restaurant
	has_many :reservations

  validates_presence_of :restaurant_id, :persons

  def available?(req_time, persons)
    start = req_time - 60.minutes
    finish = req_time + 60.minutes
    min_people = persons.to_i - 1
    max_people = persons.to_i + 2
    !reservations.today.where(time: start..finish, group_size: min_people..max_people).present?
  end
end
