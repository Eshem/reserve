class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]
  helper_method :available

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  def check_availability
    respond_to do |format|
      format.json { render json: { available: available.present? } }
    end
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    respond_to do |format|
      if available.present?
        user = User.find_by_email(params[:email])
        user = user.present? ? user : User.create(name: params[:name], email: params[:email])
        table = available.first
        r_params = { time: params[:time], group_size: params[:group_size], user_id: user.id, restaurant_id: table.restaurant_id, table_id: table.id }
        @reservation = Reservation.new(r_params)
        if @reservation.save
          format.json { render :show, status: :created, location: @reservation }
        else
          format.json { render json: @reservation.errors, status: :unprocessable_entity }
        end
      else
        format.json { render json: [] }
      end
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def available
    available = Restaurant.find_by_id(params[:restaurant_id]).tables.reject do |o|
      o.available?(Time.parse(params[:time]), params[:group_size]) == false
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:table_id, :time, :group_size)
    end
end
