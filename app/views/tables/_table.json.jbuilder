json.extract! table, :id, :name, :min_occupants, :max_occupants, :restaurant_id, :created_at, :updated_at
json.url table_url(table, format: :json)